import Promotion from './promotion.js';

class HalfPricePromotion extends Promotion {
  constructor (order) {
    super(order);
    this._type = '指定菜品半价';
    this.halfPriceDishes = ['ITEM0001', 'ITEM0022'];
  }

  get type () {
    return this._type;
  }

  includedHalfPriceDishes () {
    const includedHalfPriceDishes = this.order.itemsDetails.filter(element => this.halfPriceDishes.includes(element.id));
    if (includedHalfPriceDishes.length === 0) {
      return null;
    }
    return includedHalfPriceDishes;
  }

  discount () {
    if (this.includedHalfPriceDishes() === null) {
      return 0;
    }
    let discount = 0;
    this.includedHalfPriceDishes().map(element => {
      discount += element.price / 2 * element.count;
    });
    return discount;
  }
}

export default HalfPricePromotion;

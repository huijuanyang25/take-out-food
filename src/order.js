import dishes from './menu.js';

class Order {
  constructor (itemsMap) {
    this.itemsMap = itemsMap;
  }

  get itemsDetails () {
    const orderedItems = dishes.filter(element => this.itemsMap.has(element.id));
    const itemsDetails = orderedItems.map(element => {
      return ({ ...element, count: this.itemsMap.get(element.id) });
    });
    return itemsDetails;
  }

  get totalPrice () {
    let totalPrice = 0;
    this.itemsDetails.map(element => {
      totalPrice += element.price * element.count;
    });
    return totalPrice;
  }
}

export default Order;

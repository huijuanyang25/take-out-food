function parseInputToMap (selectedItems) {
  const splitSelectedItems = selectedItems.split(',');
  const parseInputToMap = new Map();
  splitSelectedItems.map(element => {
    parseInputToMap.set(element.substring(0, 8), parseInt(element[element.length - 1]));
  });
  return parseInputToMap;
}

export { parseInputToMap };
